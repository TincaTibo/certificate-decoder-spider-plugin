# Build bundle

FROM node:16-alpine AS build_bundle

WORKDIR /app

COPY package*.json /app/
RUN npm install --quiet --no-progress --registry=${registry:-https://registry.npmjs.org}

COPY . /app

EXPOSE 1234
ENTRYPOINT ["npm"]
CMD ["run", "dev"]
